OPEN CHARITY DRUPAL WEBSITE

ABOUT
-----
This is a Drupal 7 website built based on requirements provided. The main objective is to convert the design into a Drupal 7 theme.

CONFIGURATIONS
--------------

- Front page
Module empty_front_page is used to remove the default main content block from the front page. This is much easier and safer than editing the template files.

- Templates
Default template files are copied over from the core modules (system, node, block) to the theme folders at sites/all/themes/opencharity. This is to ensure any template customizations will be done based on default files only.

- Styles
CSS files are added into the theme's .info file, opencharity.info. The files are layout.css (for main layout structure), style.css (for style overrides and custom styles declarations) and media.css (for responsiveness). SMACSS CSS method is mainly in used.

- Responsive
A separate media.css file is used for responsiveness. Main style was written for destop screen resolution (above 1024px). Since the design and contents are very simple, only one additional media screen was added, i.e max-width 840px. So, once the screen goes below 840px, media.css will take over styling the pages.

- Slider
Slick slider and slick views slider is selected because once installed, it fits all the requirements of the design without much customizations and minimum style overrides, compared to views slideshow module and others that was tested.

- CKEditor and IMCE
These modules are used because they are much easier to install and work with compared to WYSIWYG module.

LAYOUT
------
- Only html.tpl.php and page.tpl.php template files were edited to implement the centered content with full width background layout.

- Front page
Most of contents are made of views blocks. Except the event section, which I am not sure of the what to implement there.

CONTENTS
--------
- CKEditor and IMCE allows rich contents to be added. Images can be added into the comtent using IMCE dialog.
- 2 types of content types were added. Banner is used to add banners for the main front page slider and blog is used to add blog type contents. Blog module is not used as this website only require simple contents.
 

INSTALLED CONTRIBUTED MODULES
-----------------------------
- ADMIN MENU
- BLOCK ATTRIBUTES
- CKEDITOR
- CTOOLS
- EMPTY FRONT PAGE
- ENTITY
- FIELD BLOCK
- IMCE
- JQUERY UPDATE
- LIBRARIES
- MENU ATTRIBUTES
- MODULE FILTER
- PATH AUTO
- PATH BREADCRUMBS
- SLICK
- SLICK VIEWS
- TOKEN
- VIEWS

INSTALLED LIBRARIES
-------------------
- slick


